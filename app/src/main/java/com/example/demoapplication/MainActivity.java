package com.example.demoapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
TextView textView_name;
Button btn_click;
int count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView_name = findViewById(R.id.txt_header);
        btn_click = findViewById(R.id.btn_click);
        btn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0 || count%2 ==0){
                    textView_name.setText("Pushpraj " +count);
                    count++;
                }else {
                    textView_name.setText("Anupam " +count);
                    count++;
                    Intent intent = new Intent(MainActivity.this,ButtonActivity.class);
                    intent.putExtra("Data",textView_name.getText());
                    startActivity(intent);
                }
            }
        });
    }
}